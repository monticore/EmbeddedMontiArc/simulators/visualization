#!/bin/bash
# (c) https://github.com/MontiCore/monticore  

TARGET_DIR="target/visualization.war"

SFS_VIS_DIR="/opt/SmartFoxServer_2X/SFS2X/www/visualization"

#Copy web resources to servlet destination
unzip -o $TARGET_DIR -d $SFS_VIS_DIR

#Change permissions for all files
chmod -R +x $SFS_VIS_DIR

echo Visualization deployed \(Platform Unix\)
