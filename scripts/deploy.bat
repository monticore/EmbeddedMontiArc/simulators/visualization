@rem (c) https://github.com/MontiCore/monticore  
REM Batch script

SET TARGET_DIR=target\visualization-1.0.7

SET SFS_VIS_DIR=C:\SmartFoxServer_2X\SFS2X\www\visualization

robocopy %TARGET_DIR% %SFS_VIS_DIR% /s /e

ECHO Visualization deployed \(Platform Windows\)
