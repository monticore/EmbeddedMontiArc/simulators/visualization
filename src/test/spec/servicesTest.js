/* (c) https://github.com/MontiCore/monticore */
describe('Visualization services', function() {

    var scene;
    var tLoader;
    
    //reinitialize gloabal variables for each test
    beforeEach(function () {
        scene = new THREE.Scene();
        tLoader = new THREE.TextureLoader();
    });
    
    describe('EnvBuilder', function() {
        var eb;
        
        beforeEach(function () {
            eb = new EnvBuilder(tLoader, scene);
        });
        

        it('creates EnvBuilder', function() {
            expect(eb).not.toBeNull();
        });
        it('deletes all added object', function () {
            expect(eb.clear()).toEqual(0);
        });
        /*it('adds road to scene', function () {
            let nodes = [new THREE.Vector3(0, 0, 0), new THREE.Vector3(500, 500, 1000), new THREE.Vector3(0, 0, 2000)];
            //draw road
            eb.drawRoad(nodes);
            //delete road components
            expect(eb.clear()).toBeGreaterThan(0);
        });*/
    });
    
});
