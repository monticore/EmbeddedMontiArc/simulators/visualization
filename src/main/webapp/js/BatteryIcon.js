/* (c) https://github.com/MontiCore/monticore */
// Updating batttery level and charging status based on backend server file.
function updateBattery(data) {

	if (data[0].charging) {
		let glass = document.querySelector(".glass");
		glass.innerHTML = "🔌";
	} else {
		let glass = document.querySelector(".glass");
		glass.innerHTML = "";
	}

	document.querySelectorAll(".fill").forEach(function (el) {
		//const level = Math.floor((Math.random() * 100) + 1);
		let level = data[0].battery;
		el.style.width = level + "%";
		//document.getElementById("level").innerHTML = level.toString();
		document.getElementById('battery-value').innerHTML = level + '%'
		if (level <= 20)
			el.classList.add("fill-critical");
		else
			el.classList.remove("fill-critical");

	});

}
