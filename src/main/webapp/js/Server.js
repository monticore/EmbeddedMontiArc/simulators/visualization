/* (c) https://github.com/MontiCore/monticore */
'use strict';

var Server =  {

    baseUrl: 'http://127.0.0.1:8090',
    //baseUrl: 'http://127.0.0.1:8092',
    nextFrameIdx: 0,
	errorFlagID: 0,
    simulationDoneCallback: null,
    simulationDataframes: null,

    getScenarios: function getScenarios(callback) {
        // if(!m_isConnected) return console.log("Not connected to the server!");
        // sendRequest('get_scenarios', null, callback);
        var req = new XMLHttpRequest();
        req.onload = function (req, e) {
            callback(JSON.parse(this.responseText))
        }
        req.open('get', this.baseUrl + '/scenario');
        req.send();
    },

    startSimulationWithScenario: function startSimulationWithScenario(scenarioID, numSectors, onSimulationFinished) {
        this.createSimulation(scenarioID, numSectors, onSimulationFinished);
        // this.waitUntilSimulationFinish(44, onSimulationFinished);
    },
    createSimulation: function createSimulation(scenarioID, numSectors, onSimulationFinished) {
        let formData = new FormData();
        formData.append('scenario_id', scenarioID);
        formData.append('num_sectors', numSectors);

        var req = new XMLHttpRequest();
        var self = this;
        req.onload = function (req, e) {
            var data = JSON.parse(this.responseText);
            console.log("simulation created, id=", data.id);
            self.startSimulation(data.id, onSimulationFinished);
        }
        req.open('post', this.baseUrl + '/simulation');
        req.send(formData);
    },
    startSimulation: function startSimulation(id, onSimulationFinished) {
        // start simulation
        var req = new XMLHttpRequest();
        var self = this;
        req.onload = function (req, e) {
            console.log("start simulation ", id, ": ", this.responseText);
            if (this.responseText == "ok") {
                self.waitUntilSimulationFinish(id, onSimulationFinished);
            }
        }
        req.open('post', this.baseUrl + '/simulation/' + id + '/start');
        req.send();
    },
    waitUntilSimulationFinish: function waitUntilSimulationFinish(id, onSimulationFinished) {
        var req = new XMLHttpRequest();
        var self = this;
        req.onload = function (req, e) {
            if (this.status == 200 && JSON.parse(this.responseText).result == true) {
                console.log("simulation finished");
                onSimulationFinished(id);
				self.fetchAllFrames(id);
                //self.fetchAllFrames(2);
                self.simulationDoneCallback(1000);
            } else {
                console.log("simulation not finished");
                setTimeout(function () {
                    self.waitUntilSimulationFinish(id, onSimulationFinished);
                }, 1000)
            }
        }
        req.onerror = function (ev) {
            setTimeout(function () {
                this.waitUntilSimulationFinish(id, onSimulationFinished);
            }, 1000);
        }
        req.open('get', this.baseUrl + '/simulation/' + id + '/finished');
        req.send();
    },
    fetchAllFrames: function fetchAllFrames(simulationID) {
        var req = new XMLHttpRequest();
        var self = this;
        req.onload = function (req, e) {
            var data = JSON.parse(this.responseText);
            var allFrames = [];
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].frames.length; j++) {
                    var frame = data[i].frames[j];
                    frame["id"] = data[i].id;
                    allFrames.push(frame);
                }
            }
			console.log(allFrames);
            allFrames.sort(function (a, b) {
                return a.totalTime < b.totalTime ? -1 : (a.totalTime > b.totalTime ? 1 : 0)
            });

            function groupByTotalTime(frames) {
                return frames.reduce(function (acc, x) {
                    var value = x.totalTime;
                    var el = acc.find(function (r) {
                        return r && r[0].totalTime === value
                    })
                    if (el) {
                        el.push(x)
                    } else {
                        acc.push([x])
                    }
                    return acc;
                }, [])
            }

            self.simulationDataframes = groupByTotalTime(allFrames);
            self.simulationDataframes.sort(function (a, b) {
                if (a[0].totalTime < b[0].totalTime) {
                    return -1;
                } else if (a[0].totalTime > b[0].totalTime) {
                    return 1;
                }
                return 0;
            })
		console.log(self.simulationDataframes);

        }
        req.open('get', this.baseUrl + '/simulation/' + simulationID + '/result');
        req.send();
    },

    loadMapSector: function loadMapSector(simulationID, sectorIdx, callback) {
        var self = this;
        var req = new XMLHttpRequest();
        req.onload = function (req, e) {
            var data = JSON.parse(this.responseText);
            console.log(data)
            self.loadWorld(data, callback)
        }
        req.open('get', this.baseUrl + '/simulation/' + simulationID + '/map-data/' + sectorIdx);
        req.send();
    },
    onSimulationReady: function onSimulationReady(callback) {
        // if(!m_isConnected) return console.log("Not connected to the server!");
        if (typeof callback == 'function') this.simulationDoneCallback = callback;
    },
    uploadMap: function uploadScenario(file, callback) {
        var formData = new FormData();
        formData.append('file', file);

        var req = new XMLHttpRequest();
        req.onload = function (req, e) {
            var data = JSON.parse(this.responseText);
            console.log("map uploaded.", data);
            callback();
        }
        req.open('post', this.baseUrl + '/map/');
        req.send(formData);
    },
    uploadScenario: function uploadScenario(file, callback) {
        var formData = new FormData();
        formData.append('file', file);

        var req = new XMLHttpRequest();
        req.onload = function (req, e) {
            var data = JSON.parse(this.responseText);
            console.log("scenario uploaded.", data);
            callback();
        }
        req.open('post', this.baseUrl + '/scenario/');
        req.send(formData);
    },

    uploadCarfile: function uploadCarfile(file, callback) {
        var formData = new FormData();
        formData.append('file', file);

        var req = new XMLHttpRequest();
        req.onload = function (req, e) {
            var data = JSON.parse(this.responseText);
            console.log("Car file uploaded.", data);
            callback();
        }
        req.open('post', this.baseUrl + '/car/');
        req.send(formData);
    },


    nextFrame: function nextFrame(callback) {
        if (this.nextFrameIdx >= this.simulationDataframes.length) {
            console.log("no more dataframes left");
            return;
        }

        var isInit = this.nextFrameIdx == 0 ? true : false;
        var cars = this.simulationDataframes[this.nextFrameIdx++];
        var carObjs = [];
        for (var i = 0; i < cars.length; i++) {
            var car = {};
            Object.assign(car, cars[i]);
            car.position = {x: car.posX, y: car.posY, z: car.posZ};
            car.velocity = {x: car.velocity[0], y: car.velocity[1], z: car.velocity[2]};
            car.acceleration = {x: car.acceleration[0], y: car.acceleration[1], z: car.acceleration[2]};
            car.steeringAngle = car.steering;
            carObjs.push(car);
        
        //console.log(cars);

         switch (true) {
              case car.errorFlags[0]: zeitleiste.handleErrorinZeitleiste(this.nextFrameIdx,0,self.errorFlagID,car.id);
                self.errorFlagID++;
				console.log(errorFlagID);
                break;
              case car.errorFlags[1]: zeitleiste.handleErrorinZeitleiste(this.nextFrameIdx,1,self.errorFlagID,car.id);
                self.errorFlagID++;
				console.log(errorFlagID);
                break;
              default: break;
            }

		
		}

        callback({
            cars: carObjs,
            raining: false,
            pedestrians: [],
            staticBoxObjects: [],
            isInit: isInit,
            simulationTime: cars[0].totalTime
        })
    },


   loadWorld: function(data, callback) {
        //Parse world data to JS object
        let sfsBounds = data.bounds;
        let sfsStreets = data.streets;
        let sfsBuildings = data.buildings;
        let sfsWaterways = data.waterways;
        let sfsChargingStations = data.chargingStations;

        //Getting HeightMap Data
        let sfsheightMap = data.heightMap;
        let sfsheightMapDeltaX = sfsheightMap.heightMapDeltaX;
        let sfsheightMapDeltaY = sfsheightMap.heightMapDeltaY;
        let sfsheightMapMinY = sfsheightMap.heightMapMinY;
        let sfsheightMapMinX = sfsheightMap.heightMapMinX;
        let sfsheightMapMaxY = sfsheightMap.heightMapMaxY;
        let sfsheightMapMaxX = sfsheightMap.heightMapMaxX;

        //JS world representation
        let world = {};
        //parse bounds
        world.bounds = {
            maxX: sfsBounds.maxX,
            maxY: sfsBounds.maxY,
            maxZ: sfsBounds.maxZ,
            minX: sfsBounds.minX,
            minY: sfsBounds.minY,
            minZ: sfsBounds.minZ,
            //Parse Heightmap Data
            heightMapDeltaX: sfsheightMapDeltaX,
            heightMapDeltaY: sfsheightMapDeltaY,
            heightMapMinY: sfsheightMapMinY,
            heightMapMinX: sfsheightMapMinX,
            heightMapMaxY: sfsheightMapMaxY,
            heightMapMaxX: sfsheightMapMaxX,
            allZero: true
        };
        console.log(world.bounds);

        //Parse 2D HeightMap Array
        world.bounds.heightMap = [];
        for (let i = 0; i < sfsheightMap.data.length; ++i) {
            world.bounds.heightMap.push([]);
            let ar = sfsheightMap[i];
            for (let j = 0; j < ar.length; ++j) {
                world.bounds.heightMap[i].push(ar[j]);
            }
        }

        //reusable node-parsing
        let getNodes = function getNodes(sfsNodes) {
            let nodes = [];
            for (let i = 0; i < sfsNodes.length; ++i) {
                let sfsNode = sfsNodes[i];
                //parse node
                let node = {
                    id: sfsNode.id,
                    x: sfsNode.longitude,
                    y: sfsNode.latitude,
                    z: sfsNode.altitude
                };
                //parse streetSign
                node.streetSign = {'one': false, 'two': true, 'x1': 0, 'x2': 0, 'y1': 0, 'y2': 0, 'z1': 0, 'z2': 0}
                if (sfsNode['streetSign'] != undefined) {
                    let sign = sfsNode.streetSign;
                    node.streetSign = {
                        id: sign.id,
                        type: sign.type,
                        one: sign.one,
                        two: sign.two,
                        x1: sign.x1,
                        x2: sign.x2,
                        y1: sign.y1,
                        y2: sign.y2,
                        z1: sign.z1,
                        z2: sign.z2,
                    }
                }
                //add node
                nodes.push(node);
            }
            return nodes;
        }

        //parse streets
        world.streets = [];
        for (let i = 0; i < sfsStreets.length; ++i) {
            let street = sfsStreets[i];
            world.streets.push({
                streetWidth: street.width,
                nodes: getNodes(street.nodes)
            });
        }
        //parse buildings
        world.buildings = [];
        for (let i = 0; i < sfsBuildings.length; ++i) {
            let building = sfsBuildings[i];
            world.buildings.push({nodes: getNodes(building.nodes), distance: getNodes(building.nodes)});
        }

        world.chargingStations = [];

        //comment code Below if you use old Server repo and get error,
        // the error appears because old server is not sending any chargingstation data from OSM
        // to fix pull newest Server repo 
        
		/*
        for (let i = 0; i < sfsChargingStations.length; ++i) {
            let chargingStations = sfsChargingStations[i];
            world.chargingStations.push({
                id: chargingStations.id,
                longitude: chargingStations.longitude,
                latitude: chargingStations.latitude,
                altitude: chargingStations.altitude,
                name: chargingStations.name,
                capacity: chargingStations.capacity
            });
        }
		*/

        //parse rivers
        // Uncomment code Below to use Rivers in Visualisation
		/*
        world.waterways = [];
        for (let i = 0; i < sfsWaterways.length; ++i) {
            let sfsR = sfsWaterways.getSFSObject(i);
            world.waterways.push({
                waterwayWidth: sfsR.getDouble('waterwayWidth'),
                nodes: getNodes(sfsR.getSFSArray('node'))
            });

        }
		*/
		
        console.log('World data parsed');
        console.log(world);
        callback(world)
    }
};
