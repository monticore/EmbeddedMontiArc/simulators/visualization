/* (c) https://github.com/MontiCore/monticore */
var zeitleiste = {
    carsError: [],

    buildZeitleiste: function () {
        const progressbarWrapper = document.createElement('div');
        const progressbar = document.createElement('div');
        const value = document.createElement('div');
        const numValue = document.createElement('div');
        progressbarWrapper.id = 'progWrapper';
        progressbarWrapper.classList.add('progressbarWrapper');
        progressbar.id = 'progress';
        progressbar.classList.add('progressbar');
        value.id = 'val';
        value.classList.add('value');
        numValue.classList.add('numvalue');
        numValue.id = 'numVal';

        gui.options.lf = Server.simulationDataframes.length;
        document.body.appendChild(progressbarWrapper);

        progressbarWrapper.appendChild(numValue);
        progressbarWrapper.appendChild(progressbar);
        progressbar.appendChild(value);

    },

    updateZeitleiste: function (addevent) {
        let width = (gui.options.frames * screenWidth) / gui.options.lf;

        const numericValue = document.getElementById('numVal');
        const progressWidth = document.getElementById('val');
        const bar = document.getElementById('progress');
        numericValue.innerHTML = gui.options.frames + '/' + gui.options.lf;
        progressWidth.style.width = width + 'px';
        if (addevent) {
            bar.addEventListener('click', function (e) {
                let offset = this.getClientRects()[0];
                console.log(offset);
                console.log(e);

                let x = e.clientX - offset.left;


                let ptX = Math.floor((e.clientX / bar.offsetWidth) * gui.options.lf);

                progressWidth.style.width = x + 'px';
                numericValue.innerHTML = ptX + '/' + gui.options.lf;
                WebService.WS_rewind(ptX - 2);
                WebService.WS_nextFrame();
                WebService.WS_nextFrame();

                lib.onPauseClicked();

            })
        }
    },
	
	show: function (id){
				document.getElementById('info'+id).style.visibility = 'visible'
	},
	hide: function (id){
				document.getElementById('info'+id).style.visibility = 'hidden'
	},

    handleErrorinZeitleiste: function(frame,error,id, carID){

        if(!(this.carsError.includes(carID))){
            let framecolor = document.createElement('div');
            let errorwrapper = document.createElement('div');
            let errorinfo = document.createElement('div');
            let erroricon = document.createElement('img');


            let progress = document.getElementById('progress');
            let wrapper = document.getElementById('progWrapper');

            let color = 0xff0000;
            let counter = 1;
            let car = {};
            for(let c in CARS){
                if(c == carID) {

                    switch(error){
                        case 0: CARS[c].errorflags = [[true,frame],[false,0]];
                                                break;
                        case 1:	CARS[c].errorflags = [[false,0],[true,frame]];
                                                break;
                        default: break;
                    }


                    car = c;
                    break;
                }
                else{
                    counter++;
                }
            }
            errorwrapper.appendChild(erroricon);
            errorwrapper.appendChild(errorinfo);

            wrapper.appendChild(errorwrapper);
            progress.appendChild(framecolor);


            switch(error){
                case 0: 	framecolor.classList.add('crash');
                                        erroricon.src = 'img/crash.png';
                                        errorinfo.innerHTML = car+ '  <br> crashed at Frame:'+frame;
                                        break;
                case 1:	framecolor.classList.add('leftStreet');
                                        erroricon.src = 'img/leftroad.png';
                                        errorinfo.innerHTML = car + ' <br> left Street at Frame:'+frame;

                                        break;
                default: break;
            }


        let width = (frame*screenWidth)/gui.options.lf;
        let iconID = 'icon' + id, infoID = 'info' +id ;
        erroricon.id =  iconID;
        errorinfo.id = infoID;


        erroricon.addEventListener('mouseover',function(e){
            zeitleiste.show(e.target.param);
        },false);
        erroricon.addEventListener('mouseout',function(e) {
            zeitleiste.hide(e.target.param);
        },false);

        erroricon.addEventListener('mousedown',function(e){
            lib.jumptoframe(e.target.frame);
            switchCar(car);
        },false);


        erroricon.param = id;
        erroricon.frame = frame;


        erroricon.classList.add('icon');
        errorinfo.classList.add('errInfo');

        errorwrapper.classList.add('errorwrapper')
        framecolor.style.left = width + 'px';
        errorwrapper.style.left = width + 'px';
}

        this.carsError.push(carID);


    }
};
