/* (c) https://github.com/MontiCore/monticore */
var gui = {
    grapicalUI: null,
    vehiclesettings: null,
    camerasettings: null,
    playbacksettings: null,
    options: {
        frames: 0,
        lf: 100,
        cameraHeight:160,
        cameraAngle: 3*(Math.PI/2),
        radius:300,
        model: 'veyron',
        color: 4,
        Metal: true,
        playbackSpeed: 1,
        manualPlayback: false,
        updatecamera: function (){
            camera.position.y = this.cameraHeight;
            }

    },

    createGUI: function() {
        gui.grapicalUI = new dat.GUI({autoPlace: false});
        var container = document.getElementById('my-gui-container2');
        container.appendChild(gui.grapicalUI.domElement);
    },

    createVehicleSettings: function() {
         gui.vehiclesettings = gui.grapicalUI.addFolder('Vehicle Settings');
			gui.vehiclesettings.add(gui.options, 'model', ['veyron' , 'gallardo']);
			gui.vehiclesettings.add(gui.options, 'color',{
				Orange:0,
				Blue:1,
				Red:2,
				Green:3,
				Black:4,
				White:5,
				Carmine:6,
				Gold:7,
				Bronze:8,
				Chrome:9
			});
			gui.vehiclesettings.add(gui.options, 'Metal');
            gui.vehiclesettings.open();
            gui.grapicalUI.close();
    },

    createCameraSettings: function() {
         gui.camerasettings = gui.grapicalUI.addFolder('Camera Settings');


				gui.camerasettings.add(gui.options, 'cameraHeight', 100,3000).onChange(gui.options.updatecamera);
				gui.camerasettings.add(gui.options,'cameraAngle',0,(2* Math.PI));
				gui.camerasettings.add(gui.options,'radius',300,1000);

				
				gui.camerasettings.close();
				gui.grapicalUI.close();

    },
    createPlaybackSettings: function(){
        gui.playbacksettings = gui.grapicalUI.addFolder('Playback Settings');
        gui.playbacksettings.add(gui.options, 'playbackSpeed',0,1);
        gui.playbacksettings.add(gui.options, 'manualPlayback');
        gui.playbacksettings.close();


    }
    
}
