/* (c) https://github.com/MontiCore/monticore */
var infoBox = {

  showInfoBox: function (clickedCarId) {
    document.getElementById("myInfoBox").style.width = "250px";

    if (!infoBoxCreated) {   
        var gau = infoBox.createGauge();   
        infoBox.createInfoBox();
        gau.draw();

    }
    infoBox.updateInfoBox(clickedCarId);

},

getCarInfo: function (currentCarId) {
    var car = CARS[currentCarId];

    if(mapbox){
    var newInfo = {
        velocity: car.speed / 10,
        carModel: car.modelName,
        position: car.position
    };
    }
    else{
      var newInfo = {
        velocity: car.speed / 10,
        carModel: car.name,
        position: car.position
    };

    }

    return newInfo;
},

createGauge: function () {
    var myGauge = new RadialGauge({
        renderTo: 'gauge',
        width: 240,
        height: 240,
        units: 'Km/h',
        minValue: 0,
        maxValue: 220,
        title: false,
        majorTicks: [
            '0',
            '20',
            '40',
            '60',
            '80',
            '100',
            '120',
            '140',
            '160',
            '180',
            '200',
            '220'
        ],
        minorTicks: 2,
        strokeTicks: true,
        highlights: [
            {from: 160, to: 220, color: 'rgba(200, 50, 50, .75)'}
        ],
        colorPlate: "transparent",
        colorMajorTicks: "#f5f5f5",
        colorMinorTicks: "#ddd",
        colorTitle: "#fff",
        colorUnits: "#ccc",
        colorNumbers: "#eee",
        colorNeedle: "rgba(240, 128, 128, 1)",
        colorNeedleEnd: "rgba(255, 160, 122, .9)",
        valueBox: false,
        colorNeedleShadowDown: "#333",
        borderShadowWidth: 0,
        borders: false,
        needleType: "arrow",
        needleWidth: 2,
        needleCircleSize: 7,
        needleCircleOuter: true,
        needleCircleInner: false,
        animationDuration: 1000,
        animationRule: "dequint",
        ticksAngle: 250,
        startAngle: 55,
        animatedValue: true
    });

    return myGauge;
},
setInfoBoxElements: function(){

    var infoElements = {
        "0": {
            attributeId:	"attributeIden",
            pElement: "attributeIden",
            text: "ID:  ",
            content: " ",
            unit: "",
        },
        "1": {
            attributeId: "attributeModel",
            pElement: "attributeModel",
            text: "Model: ",
            content: " ",
            unit: "" ,
        },
        "2": {
            attributeId:"attributeVelocity",
            pElement: "attributeVelocity",
            text: "Velocity: ",
            content: "0",
            unit: "Km/h",
        },
        "3": {
            attributeId:"attributePosition",
            pElement: "attributePosition",
            text: "Position (Lat, Lon): ",
            content: " ",
            unit: "",
        },
        "4": {
          attributeId:"attributeError",
          pElement: "attributeError",
          text: " ",
          content: " ",
          unit: "",
      }

    };

    return infoElements;
},


updateInfoBox: function (clickedCarId) {
  console.log(clickedCarId);

    var newCarInfo = infoBox.getCarInfo(clickedCarId);
    //console.log(clickedCarId);
    let model = newCarInfo.carModel;
    let velo = newCarInfo.velocity;
    let pos;

    if(mapbox){ pos = newCarInfo.position[0] +"<br />"+ newCarInfo.position[1];}
    else{ pos = newCarInfo.position.lat +"<br />"+ newCarInfo.position.long }


    let id = clickedCarId;
    var myvelo = parseInt(velo, 10);
    document.gauges.forEach(function(gauge) {
        gauge.value = myvelo ;
    });

    velo = velo.toFixed(2);
    var updateData = [id, model, velo, pos];
    var updateElement = infoBox.setInfoBoxElements();


    for(var m=0; m < updateData.length ;m++) {

        var attributeId = updateElement[m].attributeId;
        var attributeText = updateElement[m].text;
        updateElement[m].content = updateData[m];
        var content = updateElement[m].content;
        var unit = updateElement[m].unit;

        document.getElementById(attributeId).innerHTML = attributeText + "&nbsp;" + content + "&nbsp;"+ unit;
    }


   if(CARS[clickedCarId].errorflags) infoBox.createErrorEvent(CARS[clickedCarId]);

},


createInfoBox: function () {
        let infoboxElement = document.getElementById('myInfoBox');

        let gauge = document.getElementById("gauge");
        infoboxElement.appendChild(gauge);

        let newDetailsElement = document.createElement('details');
        let newSummaryElement = document.createElement('summary');

        let setId = "det";
        newDetailsElement.setAttribute("id", setId);
        let text = document.createTextNode("Car: ");

        infoboxElement.appendChild(newDetailsElement);
        newDetailsElement.appendChild(newSummaryElement);
        newSummaryElement.appendChild(text);

        let element = infoBox.setInfoBoxElements();

        for(let m=0; m < Object.keys(element).length ;m++){

            let attributeId = element[m].attributeId;
            let attributeText = element[m].text;

            let attribute = document.createElement('p');
            attribute.setAttribute("id", attributeId );
            newDetailsElement.appendChild(attribute);
            let text = document.createTextNode(attributeText);
            attribute.appendChild(text);

        }

        infoBoxCreated = true;
        document.getElementById("det").open = true;
},

closeInfoBox: function () {
    document.getElementById("myInfoBox").style.width = "0";

},

createErrorEvent: function (carObject) {
  console.log(carObject);
    let errorflags = carObject.errorflags;
    let errorframe, errortype;
    switch (true) {
      case errorflags[0][0]:
        errorframe = errorflags[0][1];
        errortype = "Crashed "
        break;
      case errorflags[1][0]:
        errorframe = errorflags[1][1];
        errortype = "Left Street "
      default:
        break;
    }
    document.getElementById("attributeError").innerHTML = errortype + "at frame: <i id='attributeErrorframe" + "'> " + errorframe + "</i>";
    document.getElementById("attributeErrorframe").car = carObject.name;
    document.getElementById("attributeErrorframe").addEventListener('mousedown', function (e) {
      jumptoframe(errorframe);
      switchCar(carObject.id);

    }, false);

}

};
