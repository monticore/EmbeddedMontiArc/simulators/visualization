/* (c) https://github.com/MontiCore/monticore */
var lib = {
    simCars: [],
    initNav: function () {
        WebService.WS_getScenarios(lib.onScenariosReady);

    },
    onScenariosReady: function (scenarios) {
        //prepare scenarios as dropdown data
        let scenArr = [];
        for (let i = 0; i < scenarios.length; ++i) {
            let tracksArr = [];

            scenArr.push({
                text: scenarios[i].name,
                onClick: function loadScenario() {
                    document.getElementById("loader").className = ""; //show loading screen
                    WebService.WS_onSimulationReady(lib.onSimulationReady);
                    //request scenario
                    WebService.WS_startSimulationWithScenario(scenarios[i].id, NUM_MAP_SECTOR, lib.onSimulationFinished);

                },
            });
        }

        //update controls
        NavBar([
            {
                text: USER.name,
                clas: 'fa fa-user-circle'
            },
            {
                text: 'Scenarios',
                clas: 'fa fa-file-text',
                items: scenArr
            },
            {
                text: 'Upload Map',
                clas: 'fa fa-upload',
                id: 'uploadMap', //container link
                onClick: function triggerUpload() {
                    let link = document.getElementById("uploadMap");
                    let upload = link.children[link.children.length - 1];
                    upload.click();
                }
            },
            {
                text: 'Upload Scenario',
                clas: 'fa fa-upload',
                id: 'uploadScenario', //container link
                onClick: function triggerUpload() {
                    let link = document.getElementById("uploadScenario");
                    let upload = link.children[link.children.length - 1];
                    upload.click();
                }
            },

            {
                text: 'Upload Car file',
                clas: 'fa fa-upload',
                id: 'uploadCarfile', //container link
                onClick: function triggerUpload() {
                    let link = document.getElementById("uploadCarfile");
                    let upload = link.children[link.children.length-1];
                    upload.click();
                }
            },

            {
                text: 'Logout',
                clas: 'fa fa-window-close',
                onClick: function logout() {
                    WebService.WS_logout(lib.initNav);
                }
            },
            {
                text: 'Exit',
                clas: 'fa fa-window-close',
                onClick: function redirect() {
                    window.location = 'index.html'
                }
            }


        ], document.getElementById("nav-menu"), true).then(function onNavBarReady() {
            //create upload input after NavBar is ready
            let uploadMap = document.createElement("input");
            uploadMap.setAttribute("type", "file");
            uploadMap.setAttribute("multiple", "multiple");

            uploadMap.onchange = function onChange(e) {
                $("loader").className = ""; //show loading screen
                WebService.WS_uploadMap(e.target.files[0], function onUploadDone() {
                    $("loader").className = "disabled"; //hide loading screen
                });
            }
            //add to the uplaod link
            document.getElementById("uploadMap").appendChild(uploadMap);

            // //create upload scenario form
            let uploadScenario = document.createElement("input");
            uploadScenario.setAttribute("type", "file");
            uploadScenario.setAttribute("multiple", "multiple");

            uploadScenario.onchange = function onChange(e) {
                $("loader").className = ""; //show loading screen
                WebService.WS_uploadScenario(e.target.files[0], function onUploadDone() {
                    $("loader").className = "disabled"; //hide loading screen
                });
            }

            //add to the uplaod link
            document.getElementById("uploadScenario").appendChild(uploadScenario);

            //create upload car file form
            let uploadCarfile = document.createElement("input");
            uploadCarfile.setAttribute("type", "file");
            uploadCarfile.setAttribute("multiple", "multiple");

            uploadCarfile.onchange = function onChange(e) {
                $("loader").className = ""; //show loading screen
                WebService.WS_uploadCarfile(e.target.files[0], function onUploadDone() {
                    $("loader").className = "disabled"; //hide loading screen
                });
            }
            //add to the uplaod link
            document.getElementById("uploadCarfile").appendChild(uploadCarfile);



        });

    },


    onStopClicked: function () {
        //stop simulation
        WebService.WS_stop(function onSectorLeave() {
            //fetch scenarios again and build navigation bar
            WebService.WS_getScenarios(lib.onScenariosReady);

        });
    },
	
	 animate: function () {
         if(mapbox) {

             if (DataModel.simulationTimeDeltaMs > 0) {
                 setTimeout(function () {
                     requestAnimationFrame(lib.animate);
                 }, DataModel.simulationTimeDeltaMs);
             } else {
                 requestAnimationFrame(lib.animate);
             }
         }
         else{
             animate();
         }



	},

    onSimulationFinished: function (simulationId) {
        //start animation after the environment is ready is processed
        //attach DataModel to WebSocket connection
        WebService.WS_attachListener(DataModel.onData);
        for (var i = 0; i < NUM_MAP_SECTOR; i++) {
            DataModel.onDataProcessed = lib.animate;
            WebService.WS_loadMapSector(simulationId, i, lib.onMapLoaded)
        }
    },

    onMapLoaded: function (mapData) {
        //build world
        DataModel.onData(mapData);
    },
	
	jumptoframe: function (frame){
        WebService.WS_rewind(frame-3);
        WebService.WS_nextFrame();
        WebService.WS_nextFrame();
        WebService.WS_pause();

	},


    onSimulationReady: function (duration) {
        //hide loading screen
        document.getElementById("loader").className = "disabled";
        //show message
        let sec = Math.round(duration / 1000);
        let min = Math.floor(sec / 60);
        sec -= 60 * min;

        let message = (min > 0 ? min + "m " : "") + sec + "s.";

        document.getElementById("message").innerHTML = "Simulation done for " + message + " Press play to start/resume simulation.";
        document.getElementById("message").className = "";



        //update controls
        NavBar([
            {
                text: USER.name,
                clas: 'fa fa-user-circle'
            },
            {
                text: 'Play',
                clas: 'fa fa-play',
                onClick: function play() {
                    //disable orbit controls
                    if(!mapbox) controls.enabled = false;
                    document.getElementById("message").className = "disabled";
                    WebService.WS_onUserUpdate(function updateUser(e) {
                        USER = e.user;
                    });
                    WebService.WS_start();
                    if(mapbox) lib.initCarButtons();
                }
            },
            {
                text: 'Back',
                clas: 'fa fa-step-backward',
                onClick: lib.onStopClicked
            }
        ], document.getElementById("nav-menu"), true);
    },


    initCarButtons: function () {

        if (!mapbox) {
			for (var car in CARS) {
				lib.simCars.push({
					text: car,
					onClick: function onCarSelected() {
						console.log("Selected car: " + this.text);
						if (!CARS[this.text].root) {
							loader.load(CARS[this.text].url, function (geometry) {
								createScene(geometry, this.text)
							});
						} else {
							switchCar(this.text);
						}

					}
				});
			}

        }
        else if (mapbox) {

            console.log(carNumber);
            for (var i = 0; i < carNumber;i++) {
				var car = serverData[i].id;
                lib.simCars.push({
                    text: car,
                    onClick: function onCarSelected() {
                        console.log("Selected car: " + this.text);
                        switchCar(this.text);
                    }
                });
            }

        }

        //update controls
        NavBar([
                {
                    text: USER.name,
                    clas: 'fa fa-user-circle'
                },
                {
                    text: 'Vehicles',
                    clas: 'fa fa-car',
                    items: lib.simCars
                },
                {
                    text: 'Pause',
                    clas: 'fa fa-pause',
                    onClick: lib.onPauseClicked
                },
                {
                    text: 'Stop',
                    clas: 'fa fa-stop',
                    onClick: lib.onStopClicked
                },
                {
                    text: 'CarInfo',
                    clas: 'fa fa-info-circle',
                    onClick: infoBox.showInfoBox
                 },
                {
                    text: 'Replay',
                    clas: 'fa fa-step-backward',
                    onClick: function play() {
                        //disable orbit controls
                        if(!mapbox) {                          
                            controls.enabled = false;
                        }
                        gui.options.frames = 0;
                        $("message").className = "disabled";
                        WebService.WS_onUserUpdate(function updateUser(e) {
                            USER = e.user;
                        });
                        WebService.WS_start();
                        lib.onUNPauseClicked();
                    },
                },


            ],

            document.getElementById("nav-menu"), true);

            if(mapbox){
                if(!createdProgressbar){
                    //Zeitleiste HTML Strukut aufbauen
                    zeitleiste.buildZeitleiste();
                    createdProgressbar = true;
                }

            }
            else{
                //Zeitleiste HTML Strukut aufbauen
                zeitleiste.buildZeitleiste();
                update_datcontrols();

            }

        },
		
		onUNPauseClicked: function () {
			//Pause simulation animations
			WebService.WS_rewind(gui.options.frames);
			//WebService.WS_continue();
			

			// Update Navbar
			NavBar([
				{
					text: USER.name,
					clas: 'fa fa-user-circle'
				},
				{
					text: 'Vehicles',
					clas: 'fa fa-car',
					items: lib.simCars
				},
				{
					text: 'Pause',
					clas: 'fa fa-pause',
					onClick: lib.onPauseClicked
				},
				{
					text: 'Stop',
					clas: 'fa fa-stop',
					onClick: lib.onStopClicked
				},
				{
					text: 'CarInfo',
					clas: 'fa fa-info-circle',
					onClick: infoBox.showInfoBox
				},
				{
					text: 'Replay',
					clas: 'fa fa-step-backward',
					onClick: function play() {
						//disable orbit controls
						if(!mapbox) {
							controls.enabled = false;
						}
						gui.options.frames = 0;
						$("message").className = "disabled";
						WebService.WS_onUserUpdate(function updateUser(e) {
							USER = e.user;
						});
						WebService.WS_start();

					},
				},
			], document.getElementById("nav-menu"), true);
		},
		
		onPauseClicked: function () {
			//Pause simulation animations
			WebService.WS_pause();
			// Update Navbar
			NavBar([
				{
					text: USER.name,
					clas: 'fa fa-user-circle'
				},
				{
					text: 'Vehicles',
					clas: 'fa fa-car',
					items: lib.simCars
				},
				{
					text: 'Continue/Play',
					clas: 'fa fa-play',
					onClick: lib.onUNPauseClicked
				},
				{
					text: 'Stop',
					clas: 'fa fa-stop',
					onClick: lib.onStopClicked
				},
				{
					text: 'CarInfo',
					clas: 'fa fa-info-circle',
					onClick: infoBox.showInfoBox
				},
				{
					text: 'Replay',
					clas: 'fa fa-step-backward',
					onClick: function play() {
						//disable orbit controls
						if(!mapbox) {
							controls.enabled = false;
						}
						gui.options.frames = 0;
						$("message").className = "disabled";
						WebService.WS_onUserUpdate(function updateUser(e) {
							USER = e.user;
						});

						WebService.WS_start();
                        lib.onUNPauseClicked();
					},
				},
			], document.getElementById("nav-menu"), true);
         },

         


};
