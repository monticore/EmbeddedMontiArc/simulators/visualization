/* (c) https://github.com/MontiCore/monticore */
'use static';

function MapboxEnvironment(mapCenter){

	var map = new mapboxgl.Map({
		container: 'map',
		style: 'mapbox://styles/kusmenko/ck1hujvpz1q3h1co82n8537i5',
		zoom: 18,
		center: mapCenter,
		pitch: 0,
		bearing:0,
		antialias: true
	});
	
	return map;
}
