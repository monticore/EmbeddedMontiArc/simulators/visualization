/* (c) https://github.com/MontiCore/monticore */
'use strict';

const Server = (function Server() {

    // Create configuration object
    let config = {
        host: window.location.hostname,
        port: 80,
        debug: false,
        useSSL: false,
        zone: 'World'
    };
    var self = {};
    // Create SmartFox client instance
    let sfs = new SFS2X.SmartFox(config);
    let simulationDataframes = [];
    self.simulationDataframes = simulationDataframes;
    let nextFrameIdx = 0;

    //private members
    let m_isConnected = false;
    let m_room = null;
    let m_user = null;

    let m_pingPonger = {
        atRate: 60, //sec
        interval: null, //interval reference
        start: function start() {
            this.interval = setInterval(function doPingPong() {
                if (!m_isConnected) return m_pingPonger.stop();
                sendRequest("ping_pong", null, console.log);
                console.log("Ping");
            }, this.atRate * 1000);
        },
        stop: function stop() {
            clearInterval(this.interval);
            console.log("Stop ping-pong");
        }
    }


    //callback references
    let m_listeners = {
        onLogin: function (e) {
            m_user = e.user;
            m_pingPonger.start();

            if (typeof m_listeners.onLoginCustom == 'function') m_listeners.onLoginCustom(m_user);
        },
        onLoginCustom: null,
        onLoginError: function () {
        },
        onLogout: function () {
        },
        onUserExitRoom: function (e) { //initial handler
            if (e != null && e.room == m_room && e.user == m_user) {
                console.log("Disconnecting from sector");
                m_room = null;
            }

            if (typeof m_listeners.onUserExitRoomCustom == 'function') m_listeners.onUserExitRoomCustom(e);
        },
        onUserExitRoomCustom: null,
        onUserVarsUpdate: function () {
        }
    };
    let m_extensionListeners = {};


    //main listeners
    let onConnection = function onConnection() {
        m_isConnected = true;
    };
    let onConnectionLost = function onConnectionLost() {
        //clean up
        sfs.removeEventListener(SFS2X.SFSEvent.CONNECTION, onConnection);
        sfs.removeEventListener(SFS2X.SFSEvent.CONNECTION_LOST, onConnectionLost);

        sfs.removeEventListener(SFS2X.SFSEvent.ROOM_JOIN, onRoomJoined, this);
        sfs.removeEventListener(SFS2X.SFSEvent.ROOM_JOIN_ERROR, onRoomJoinError, this);

        sfs.removeEventListener(SFS2X.SFSEvent.LOGIN, m_listeners.onLogin, this);
        sfs.removeEventListener(SFS2X.SFSEvent.LOGIN_ERROR, m_listeners.onLoginError, this);
        sfs.removeEventListener(SFS2X.SFSEvent.LOGOUT, m_listeners.onLogout, this);

        sfs.removeEventListener(SFS2X.SFSEvent.USER_EXIT_ROOM, m_listeners.onUserExitRoom, this);

        sfs.removeEventListener(SFS2X.SFSEvent.EXTENSION_RESPONSE, onExtensionResponse, this);

        //clean up callback references
        m_listeners = null;
        m_extensionListeners = null;
        m_isConnected = false;
        m_room = null;

        console.log('Disconnected from the server!');

        m_pingPonger.stop();

        let msg = document.getElementById("loader");
        msg.innerHTML = "You've been disconnected from the server.<br/>Please reload the page to connect again.";
        msg.className = "";
    };
    let onRoomJoined = function onRoomJoined(e) {
        m_room = e.room;
    };
    let onRoomJoinError = function onRoomJoinError(e) {
        m_room = null;
    };

    let onExtensionResponse = function onExtensionResponse(e) {
    }

    //private methods
    let sendRequest = function sendRequest(cmd, params, callback) {
        if (typeof callback == 'function') m_extensionListeners[cmd] = callback;
        sfs.send(new SFS2X.ExtensionRequest(cmd, params, m_room));
    };

    let loadWorld = function loadWorld(data, callback) {
        //Parse world data to JS object
        let sfsBounds = data.bounds;
        let sfsStreets = data.streets;
        let sfsBuildings = data.buildings;
        let sfsWaterways = data.waterways;

        //Getting HeightMap Data
        let sfsheightMap = data.heightMap;
        let sfsheightMapDeltaX = sfsheightMap.heightMapDeltaX;
        let sfsheightMapDeltaY = sfsheightMap.heightMapDeltaY;
        let sfsheightMapMinY = sfsheightMap.heightMapMinY;
        let sfsheightMapMinX = sfsheightMap.heightMapMinX;
        let sfsheightMapMaxY = sfsheightMap.heightMapMaxY;
        let sfsheightMapMaxX = sfsheightMap.heightMapMaxX;

        //JS world representation
        let world = {};
        //parse bounds
        world.bounds = {
            maxX: sfsBounds.maxX,
            maxY: sfsBounds.maxY,
            maxZ: sfsBounds.maxZ,
            minX: sfsBounds.minX,
            minY: sfsBounds.minY,
            minZ: sfsBounds.minZ,
            //Parse Heightmap Data
            heightMapDeltaX: sfsheightMapDeltaX,
            heightMapDeltaY: sfsheightMapDeltaY,
            heightMapMinY: sfsheightMapMinY,
            heightMapMinX: sfsheightMapMinX,
            heightMapMaxY: sfsheightMapMaxY,
            heightMapMaxX: sfsheightMapMaxX,
            allZero: true
        };
        console.log(world.bounds);

        //Parse 2D HeightMap Array
        world.bounds.heightMap = [];
        for (let i = 0; i < sfsheightMap.data.length; ++i) {
            world.bounds.heightMap.push([]);
            let ar = sfsheightMap[i];
            for (let j = 0; j < ar.length; ++j) {
                world.bounds.heightMap[i].push(ar[j]);
            }
        }

        //reusable node-parsing
        let getNodes = function getNodes(sfsNodes) {
            let nodes = [];
            for (let i = 0; i < sfsNodes.length; ++i) {
                let sfsNode = sfsNodes[i];
                //parse node
                let node = {
                    id: sfsNode.id,
                    x: sfsNode.longitude,
                    y: sfsNode.latitude,
                    z: sfsNode.altitude
                };
                //parse streetSign
                node.streetSign = {'one': false, 'two': true, 'x1': 0, 'x2': 0, 'y1': 0, 'y2': 0, 'z1': 0, 'z2': 0}
                if (sfsNode['streetSign'] != undefined) {
                    let sign = sfsNode.streetSign;
                    node.streetSign = {
                        id: sign.id,
                        type: sign.type,
                        one: sign.one,
                        two: sign.two,
                        x1: sign.x1,
                        x2: sign.x2,
                        y1: sign.y1,
                        y2: sign.y2,
                        z1: sign.z1,
                        z2: sign.z2,
                    }
                }
                //add node
                nodes.push(node);
            }
            return nodes;
        }

        //parse streets
        world.streets = [];
        for (let i = 0; i < sfsStreets.length; ++i) {
            let street = sfsStreets[i];
            world.streets.push({
                streetWidth: street.width,
                nodes: getNodes(street.nodes)
            });
        }
        //parse buildings
        world.buildings = [];
        for (let i = 0; i < sfsBuildings.length; ++i) {
            let building = sfsBuildings[i];
            world.buildings.push({nodes: getNodes(building.nodes), distance: getNodes(building.nodes)});
        }

        //parse rivers
        // world.waterways = [];
        // for (let i = 0; i < sfsWaterways.length; ++i) {
        //     let sfsR = sfsWaterways.getSFSObject(i);
        //     world.waterways.push({
        //         waterwayWidth: sfsR.getDouble('waterwayWidth'),
        //         nodes: getNodes(sfsR.getSFSArray('node'))
        //     });

        // }
        console.log(world.buildings);

        console.log('World data parsed');
        console.log(world);
        callback(world)
    }


    self = {
        baseUrl: 'http://127.0.0.1:8090',
        //extension requests - Zone
        getScenarios: function getScenarios(callback) {
            // if(!m_isConnected) return console.log("Not connected to the server!");
            // sendRequest('get_scenarios', null, callback);
            var req = new XMLHttpRequest();
            req.onload = function (req, e) {
                callback(JSON.parse(this.responseText))
            }
            req.open('get', self.baseUrl + '/scenario');
            req.send();
        },
        loadScenario: function loadScenario(scenarioId, trackId, callback) {
            if (!m_isConnected) return console.log("Not connected to the server!");
            if (!scenarioId || !trackId) return console.log("Missing parameter: scenarioId/trackId");
            var params = new SFS2X.SFSObject();
            params.putInt("scenarioId", scenarioId);
            params.putLong("trackId", trackId);

            sendRequest('load_scenario', params, callback);
        },
        startSimulationWithScenario: function startSimulationWithScenario(scenarioID, numSectors, onSimulationFinished) {
            self.createSimulation(scenarioID, numSectors, onSimulationFinished);
            // self.waitUntilSimulationFinish(44, onSimulationFinished);
        },
        createSimulation: function createSimulation(scenarioID, numSectors, onSimulationFinished) {
            let formData = new FormData();
            formData.append('scenario_id', scenarioID);
            formData.append('num_sectors', numSectors);

            var req = new XMLHttpRequest();
            req.onload = function (req, e) {
                var data = JSON.parse(this.responseText);
                console.log("simulation created, id=", data.id);
                self.startSimulation(data.id, onSimulationFinished);
            }
            req.open('post', self.baseUrl + '/simulation');
            req.send(formData);
        },
        startSimulation: function startSimulation(id, onSimulationFinished) {
            // start simulation
            var req = new XMLHttpRequest();
            req.onload = function (req, e) {
                console.log("start simulation ", id, ": ", this.responseText);
                if (this.responseText == "ok") {
                    self.waitUntilSimulationFinish(id, onSimulationFinished);
                }
            }
            req.open('post', self.baseUrl + '/simulation/' + id + '/start');
            req.send();
        },
        waitUntilSimulationFinish: function waitUntilSimulationFinish(id, onSimulationFinished) {
            var req = new XMLHttpRequest();
            req.onload = function (req, e) {
                var data = JSON.parse(this.responseText);
                if (data.result == true) {
                    console.log("simulation finished");
                    onSimulationFinished(id);
                    self.fetchAllFrames(id);
                    m_extensionListeners['sim_done'](1000)
                } else {
                    console.log("simulation not finished");
                    setTimeout(function () {
                        self.waitUntilSimulationFinish(id, onSimulationFinished);
                    }, 1000)
                }
            }
            req.onerror = function (ev) {
                setTimeout(function () {
                    self.waitUntilSimulationFinish(id, onSimulationFinished);
                }, 1000);
            }
            req.open('get', self.baseUrl + '/simulation/' + id + '/finished');
            req.send();
        },
        fetchAllFrames: function fetchAllFrames(simulationID) {
            var req = new XMLHttpRequest();
            req.onload = function (req, e) {
                var data = JSON.parse(this.responseText);
                var allFrames = [];
                for (var i = 0; i < data.length; i++) {
                    for (var j = 0; j < data[i].frames.length; j++) {
                        var frame = data[i].frames[j];
                        frame["id"] = data[i].id;
                        allFrames.push(frame);
                    }
                }
                allFrames.sort(function (a, b) {
                    return a.totalTime < b.totalTime ? -1 : (a.totalTime > b.totalTime ? 1 : 0)
                });

                function groupByTotalTime(frames) {
                    return frames.reduce(function (acc, x) {
                        var value = x.totalTime;
                        var el = acc.find(function (r) {
                            return r && r[0].totalTime === value
                        })
                        if (el) {
                            el.push(x)
                        } else {
                            acc.push([x])
                        }
                        return acc;
                    }, [])
                }

                Server.simulationDataframes = groupByTotalTime(allFrames);
                self.simulationDataframes = Server.simulationDataframes;
                Server.simulationDataframes.sort(function (a, b) {
                    if (a[0].totalTime < b[0].totalTime) {
                        return -1;
                    } else if (a[0].totalTime > b[0].totalTime) {
                        return 1;
                    }
                    return 0;
                })

            }
            req.open('get', self.baseUrl + '/simulation/' + simulationID + '/result');
            req.send();

        },
        loadMapSector: function loadMapSector(simulationID, sectorIdx, callback) {
            var req = new XMLHttpRequest();
            req.onload = function (req, e) {
                var data = JSON.parse(this.responseText);
                console.log(data)
                loadWorld(data, callback)
            }
            req.open('get', self.baseUrl + '/simulation/' + simulationID + '/map-data/' + sectorIdx);
            req.send();
        },
        onSimulationReady: function onSimulationReady(callback) {
            // if(!m_isConnected) return console.log("Not connected to the server!");
            if (typeof callback == 'function') m_extensionListeners['sim_done'] = callback;
        },
        uploadMap: function uploadScenario(file, callback) {
            var formData = new FormData();
            formData.append('file', file);

            var req = new XMLHttpRequest();
            req.onload = function (req, e) {
                var data = JSON.parse(this.responseText);
                console.log("map uploaded.", data);
                callback();
            }
            req.open('post', self.baseUrl + '/map/');
            req.send(formData);
        },
        uploadScenario: function uploadScenario(file, callback) {
            var formData = new FormData();
            formData.append('file', file);

            var req = new XMLHttpRequest();
            req.onload = function (req, e) {
                var data = JSON.parse(this.responseText);
                console.log("scenario uploaded.", data);
                callback();
            }
            req.open('post', self.baseUrl + '/scenario/');
            req.send(formData);
        },
        //extension requests - Sector
        leaveRoom: function leaveRoom(callback) {
            if (!m_isConnected) return console.log("Not connected to server/sector!");

            m_listeners.onUserExitRoomCustom = callback;

            if (!m_room) m_listeners.onUserExitRoom(); //user has already left the room
            else sfs.send(new SFS2X.LeaveRoomRequest(m_room));
        },
        nextFrame: function nextFrame(callback) {

            if (self.nextFrameIdx >= Server.simulationDataframes.length) {

              console.log("No Frames available anymore, Simulation finished");

                return;
            }

            var isInit = self.nextFrameIdx == 0 ? true : false;
            var cars = Server.simulationDataframes[self.nextFrameIdx++];
            var carObjs = [];
            for (var i = 0; i < cars.length; i++) {
                var car = {};
                Object.assign(car, cars[i]);
                car.position = {x: car.posX, y: car.posY, z: car.posZ};
                car.velocity = {x: car.velocity[0], y: car.velocity[1], z: car.velocity[2]};
                car.acceleration = {x: car.acceleration[0], y: car.acceleration[1], z: car.acceleration[2]};
                car.steeringAngle = car.steering;
                carObjs.push(car);
            }
            console.log(cars);
            callback({
                cars: carObjs,
                raining: false,
                pedestrians: [],
                staticBoxObjects: [],
                isInit: isInit,
                simulationTime: cars[0].totalTime
            })
        },
    };

    return self;
})();
