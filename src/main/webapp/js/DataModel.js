/* (c) https://github.com/MontiCore/monticore */
'use strict';


/**
*   Singleton object. Handles the data received from server calls.
*   Contains listeners, which parse the data and execute business logic
*   in order to build environment or control the vehicles.
*/
const DataModel = (function DataModel() {

    var self = {};

    var DEF_MIN_LAT = 0;
    var DEF_MIN_LONG = 0;

    var DEF_LAT_CONSTANT = 110.574;
    var DEF_LONG_CONSTANT = 111.320;



    var DEF_BLOCK_SIZE = 100;

    // Timing values
    self.simulationTimePrevMs = 0;
    self.simulationTimeCurrMs = 0;
    self.simulationTimeDeltaMs = 0;

    //match the data keys from the server
    self.STREETS_HANDLER = "streets";
    self.BUILDINGS_HANDLER = "buildings";
    self.CARS_HANDLER = "cars";
    self.PEDESTRIANS_HANDLER = "pedestrians";
    self.BOUNDS_HANDLER = "bounds";
    self.RAIN_HANDLER = "raining";
    self.STATIC_BOX_OBJECT_HANDLER = "staticBoxObjects";
    self.CHARGING_STATION_HANDLER = "chargingStations";
    self.CARS_HANDLER_MAPBOX = "cars";

    self.RIVER_HANDLER = "waterways";
    self.TREE_HANDLER = "trees";

    self.onDataProcessed; //to be attached
    self.afterEachFrame; //to be attached

    var DATA_HANDLERS = {};

    //custom parsers - parse the server data to the structured data we need
    var PARSERS = {};

    //simulation control
    var cachedPrevFrame;

    var toRadians = (degrees) => {
      return degrees * Math.PI / 180;
    }

    //config
    var parseCoordinates = (point, fn) => {
        if(typeof fn !== "function") fn = (a) => { return a; };
        return { x: fn(point.x), y: fn(point.z), z: fn(point.y) };
    };

    var getMeters = (a) => { return a * DEF_BLOCK_SIZE; };



    var convertLattoPixel = (lat,lon) => {
      var x = 1000*(lat - DEF_MIN_LAT)* DEF_LAT_CONSTANT;
      return x;
    }

    var convertLontoPixel = (lat,lon) => {
      var  y = 1000* (lon - DEF_MIN_LONG) * (DEF_LONG_CONSTANT * Math.cos(toRadians(lat)));
      return y;
    }
    // TODO: Implement function that parses lon and lat to pixel for three JS visualisation

    PARSERS[self.RIVER_HANDLER] = function (rivers) {
      var x,y,z;
      for(var i=0; i<rivers.length; ++i) {
          var nodes = rivers[i].nodes;
          for(var n=0; n<nodes.length; ++n) {
            x = convertLattoPixel(nodes[n].x,nodes[n].y);
            y = convertLontoPixel(nodes[n].x,nodes[n].y);
            z = nodes[n].z


              nodes[n].x = getMeters(x);
              nodes[n].y = getMeters(z);
              nodes[n].z = getMeters(y);
          }
        rivers[i].waterwayWidth = getMeters(rivers[i].waterwayWidth);
      }
      return rivers;
    }

    PARSERS[self.BUILDINGS_HANDLER] = function (buildings) {
      var x,y,z;
      for(var i=0; i<buildings.length; ++i) {
          var nodes = buildings[i].nodes;
          for(var n=0; n<nodes.length; ++n) {
              x = convertLattoPixel(nodes[n].x,nodes[n].y);
              y = convertLontoPixel(nodes[n].x,nodes[n].y);
              z = nodes[n].z

              nodes[n].x = getMeters(x);
              nodes[n].y = getMeters(z);
              nodes[n].z = getMeters(y);
          }
      }
      console.log(buildings);

      console.log(nodes);
      return buildings;
    }

    PARSERS[self.CHARGING_STATION_HANDLER] = function (chargingStations) {
        var x, y, z;
        for (var i = 0; i < chargingStations.length; ++i) {
          x = convertLattoPixel(chargingStations[i].longitude, chargingStations[i].latitude);
          y = convertLontoPixel(chargingStations[i].longitude, chargingStations[i].latitude);
          z = chargingStations[i].altitude;
    
          chargingStations[i].x = getMeters(x);
          chargingStations[i].y = getMeters(z);
          chargingStations[i].z = getMeters(y);
    
    
        }
        return chargingStations;
      }

    PARSERS[self.STREETS_HANDLER] = function (streets) {
      var x,y,z;
        for(var i=0; i<streets.length; ++i) {
            var nodes = streets[i].nodes;
            for(var n=0; n<nodes.length; ++n) {
                //var pt = parseCoordinates(nodes[n], getMeters);

                 x = convertLattoPixel(nodes[n].x,nodes[n].y);
                 y = convertLontoPixel(nodes[n].x,nodes[n].y);
                 z = nodes[n].z


                nodes[n].x = getMeters(x);
                nodes[n].y = getMeters(z);
                nodes[n].z = getMeters(y);
				if(nodes[n].streetSign.one) {
					nodes[n].streetSign.x1 = getMeters(nodes[n].streetSign.x1);
					nodes[n].streetSign.z1 = getMeters(nodes[n].streetSign.y1);
					nodes[n].streetSign.y1 = 0;
				}
				if(nodes[n].streetSign.two) {
					nodes[n].streetSign.x2 = getMeters(nodes[n].streetSign.x2);
					nodes[n].streetSign.z2 = getMeters(nodes[n].streetSign.y2);
					nodes[n].streetSign.y2 = 0;
				}
            }
            streets[i].streetWidth = getMeters(streets[i].streetWidth);
        }

        return streets;
    }

    PARSERS[self.BOUNDS_HANDLER] = function (bounds) {
        for(var k in bounds){
          if(Array.isArray(bounds[k])){
            break;
          }
      };

        var minX= bounds.minX
        var maxX= bounds.maxX
        var minY= bounds.minY
        var maxY= bounds.maxY
        // Should heightMapMax- Min and Delta  data be converted into Meters?
          bounds.maxX = getMeters(convertLattoPixel(maxX,maxY,minX));
          bounds.minX = getMeters(convertLattoPixel(minX,minY,minX));



          bounds.maxY = getMeters(convertLontoPixel(maxX,maxY,minY));
          bounds.minY = getMeters(convertLontoPixel(minX,minY,minY));


          let heightmap = bounds.heightMap;
          // Convert Data in 2D Array from Lat and Lon to Meters
          if (heightmap.length == 0) {
            bounds.allZero = true;
          }

          if (!bounds.allZero) {
            let deltaX = bounds.heightMapDeltaX;
            let deltaY = bounds.heightMapDeltaY;
            console.log("deltaX=" + deltaX + ",deltaY=" + deltaY);


            // bounds.minY is position at top and therefore the highest y value in this coordinate system
            console.log(bounds);
            let nMinX =Math.floor((bounds.minX - bounds.heightMapMinX) /deltaX);
            //let nMinY = Math.floor((bounds.heightMapMaxY - bounds.minY)/ delta);
            let nMinY = Math.floor((bounds.minY - bounds.heightMapMinY) / deltaY);
            //let nMaxX = Math.ceil((bounds.heightMapMaxX - bounds.maxX)/ delta);
            let nMaxX = Math.ceil((bounds.maxX - bounds.heightMapMinX) / deltaX);
            //let nMaxY = Math.ceil((bounds.maxY - bounds.heightMapMinY)/ delta);
            let nMaxY = Math.ceil((bounds.maxY - bounds.heightMapMinY) / deltaY);
            console.log("minX=" + nMinX + ",minY=" + nMinY + ",maxX=" + nMaxX + ",maxY=" + nMaxY);

            let stretchedHeightMap = [];
            for (let row = nMinY; row <= nMaxY; row++) {
                let l = [];
                for (let col = nMinX; col <= nMaxX; col++) {
                    l.push(bounds.heightMap[row][col]);
                }
                stretchedHeightMap.push(l);
            }


            /*for (let i = nMinX - 1, x=0; i < nMaxX; i++,x++) {
              stretchedHeightMap.push([]);
              for (let j = nMinY - 1; j < nMaxY; j++) {
                  stretchedHeightMap[x].push(bounds.heightMap[i][j]);
              }
            }*/




            //flatten the 2D Array to a 1D Array
            bounds.flatHeightMap = [];
            for (let i =0; i<stretchedHeightMap.length;++i ){
              for(let j = 0; j < stretchedHeightMap[i].length; ++j){
                bounds.flatHeightMap.push(stretchedHeightMap[i][j]);
              }
            }



            //Convert Meters to Visualization Meters
            for (let i = 0; i < bounds.flatHeightMap.length;++i){
              bounds.flatHeightMap[i] = getMeters(bounds.flatHeightMap[i]);
            }
            console.log(bounds.flatHeightMap);
          }
          else {
            bounds.flatHeightMap = 0;
          }




        return bounds;
    }

    PARSERS[self.CARS_HANDLER_MAPBOX] = function (cars) {
        for(var i=0; i<cars.length; ++i){
          cars[i].position = (cars[i].posX, cars[i].posY,0);


          cars[i].velocity = parseCoordinates(cars[i].velocity, getMeters);
          cars[i].acceleration = parseCoordinates(cars[i].acceleration, getMeters);
        }

      return cars
    }

    PARSERS[self.CARS_HANDLER] = function (cars) {
        for(var i=0; i<cars.length; ++i){
            //cars[i].position = parseCoordinates(cars[i].position, getMeters);



            let x = cars[i].position.x;
            let y = cars[i].position.y;
            let z = cars[i].position.z;
            console.log(x,y);


             cars[i].position.x =  getMeters(convertLattoPixel(x,y));
             cars[i].position.y =  getMeters(z);
             cars[i].position.z =  getMeters(convertLontoPixel(x,y));

             cars[i].position.long = x;
             cars[i].position.lat =  y;




            // cars[i].position = parseCoordinates(cars[i].position, getMeters);
//            console.log(cars[i].position);

            cars[i].speed = cars[i].velocity
            cars[i].velocity = parseCoordinates(cars[i].velocity, getMeters);
            cars[i].acceleration = parseCoordinates(cars[i].acceleration, getMeters);
        }
        return cars;
    }

	PARSERS[self.PEDESTRIANS_HANDLER] = function (pedestrians) {
        for(var i=0; i<pedestrians.length; ++i) {
			if(typeof pedestrians[i] != 'undefined') pedestrians[i].position = parseCoordinates(pedestrians[i].position, getMeters);
        }
        return pedestrians;
    }

    PARSERS[self.RAIN_HANDLER] = function (bool) { //FIXME remove
        return bool;
    }

/*    PARSERS[self.BUILDINGS_HANDLER] = function (buildings) {
      for(let i=0; i<building.length; ++i){
        if(typeof buildings[i] != 'undefined') {
          var x = staticBoxObjects[i].position.x * 100;
          var y = staticBoxObjects[i].position.z * 100;
          var z = staticBoxObjects[i].position.y * 100;
          staticBoxObjects[i].position.x = x
          staticBoxObjects[i].position.y = y;
          staticBoxObjects[i].position.z = z;
        }
      }
    }*/

    PARSERS[self.STATIC_BOX_OBJECT_HANDLER] = function (staticBoxObjects) {
        for(var i = 0; i < staticBoxObjects.length; ++i){
            var x = staticBoxObjects[i].position.x * 100;
            var y = staticBoxObjects[i].position.z * 100;
            var z = staticBoxObjects[i].position.y * 100;
            staticBoxObjects[i].position.x = x
            staticBoxObjects[i].position.y = y;
            staticBoxObjects[i].position.z = z;
        }

        return staticBoxObjects;
    }

    //initialize simulation after getting the required data
    var initialize = function initialize() {
        console.log('INITIALIZE SIMULATION ANIMATION');
        if(self.onDataProcessed) self.onDataProcessed();
        self.onDataProcessed = null; //executed only once
    }

    var onHandlersDone = function onHandlersDone() {
        if(typeof self.afterEachFrame == "function") self.afterEachFrame(); //e.g. send a screenshot
        WebService.WS_nextFrame();
    }

    var handleData = function handleData(data, initialized) {

        // Timing values
        self.simulationTimePrevMs = self.simulationTimeCurrMs;
        self.simulationTimeCurrMs = data.simulationTime;
        self.simulationTimeDeltaMs = self.simulationTimeCurrMs - self.simulationTimePrevMs;

        //execute specific handler with specific data
        var promises = [];
        for(var h in DATA_HANDLERS) {
            if(DATA_HANDLERS[h] && (data[h] != null)) {

                promises.push( DATA_HANDLERS[h](PARSERS[h] ? PARSERS[h](data[h]) : data[h]) );
            }
        }

        //initialize the simulation (e.g. run 'animate' if we have received the environment data)
        //otherwise ask for the next frame, when all the handlers have called resolve, i.e. they have performed not only calculations, but also executing animations
        Promise.all(promises).then(initialized ? onHandlersDone : initialize);
    }

    var cacheData = function cacheData(data) {
        cachedPrevFrame = Object.assign({}, data);
    }


    self.addHandler = function addHandler(type, handler, mapbox) {


        //Only the handlers registered here will be executed, when server data is available !

        if (mapbox){
          var handlers = [self.CARS_HANDLER_MAPBOX];

        }
        else{
        var handlers = [self.RIVER_HANDLER, self.STREETS_HANDLER, self.BUILDINGS_HANDLER, self.CARS_HANDLER, self.BOUNDS_HANDLER, self.RAIN_HANDLER, self.PEDESTRIANS_HANDLER, self.STATIC_BOX_OBJECT_HANDLER,self.CHARGING_STATION_HANDLER];
        }
        if( (handlers.findIndex( (el) => { return el == type; }) > -1) && typeof handler == "function") {
                DATA_HANDLERS[type] = function (data) {
                    return new Promise(function (resolve, reject) {
                        handler(data, resolve, reject);
                    });
                }
        }
    }

    self.onData = function onData(data) {
      if(data.bounds){
      DEF_MIN_LAT = data.bounds.minX;
      DEF_MIN_LONG = data.bounds.minY;
      console.log(DEF_MIN_LAT);
    }


        if(!data) return; //stop simulation

        if(typeof self.onDataProcessed == "function") {
            console.log('Init simulation');
            handleData(data, false); //init data load, run initialize() at the end
        }else if(!cachedPrevFrame) {
          //  console.log('Init cache');
            cacheData(data); //no cached data, cache current and ask for next frame
            WebService.WS_nextFrame();
        }else {
          //  console.log('Play and cache');
            handleData(cachedPrevFrame, true); //initialized and prev frame is cached, execute prev frame
            cacheData(data); //cache current frame data
        }
    }

      console.log(self);
    return self;
})();
