/* (c) https://github.com/MontiCore/monticore */
'use strict';

function CarObject() {

    this.modelScale = 1;

    this.type = null;
    this.id = null;
    this.modelName = null;
    this.model = null;
    this.speed = 0;
    this.acceleration = 0;
    this.position = (0,0,0);
    this.rotation = (0,0,0);


    // this.wheelOrientation = 0;
    // this.carOrientation = 0;

    this.MAX_SPEED = 2200;
    this.MAX_REVERSE_SPEED = -1500;

    this.MAX_WHEEL_ROTATION = 0.6;

    this.FRONT_ACCELERATION = 1250;
    this.BACK_ACCELERATION = 1500;

    this.WHEEL_ANGULAR_ACCELERATION = 1.5;

    this.FRONT_DECCELERATION = 750;
    this.WHEEL_ANGULAR_DECCELERATION = 1.0;

    this.STEERING_RADIUS_RATIO = 0.0023;

    this.MAX_TILT_SIDES = 0.05;
    this.MAX_TILT_FRONTBACK = 0.015;


}

